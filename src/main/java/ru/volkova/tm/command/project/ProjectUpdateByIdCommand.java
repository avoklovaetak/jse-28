package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update project by id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Optional<Project> projectUpdated = serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description);
        if (!projectUpdated.isPresent()) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
