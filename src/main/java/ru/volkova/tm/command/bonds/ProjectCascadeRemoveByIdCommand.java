package ru.volkova.tm.command.bonds;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

public class ProjectCascadeRemoveByIdCommand extends AbstractProjectTaskClass {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "remove project and all project tasks by id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[REMOVE PROJECT AND ALL TASKS CASCADE]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectTaskService()
                .removeProjectById(userId, projectId);
    }

    @NotNull
    @Override
    public String name() {
        return "project-cascade-remove-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
